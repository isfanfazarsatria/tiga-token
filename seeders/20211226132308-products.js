'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
       await queryInterface.bulkInsert('Products', [{
          id: 1,
          name: 'Product 1',
          qty: 10,
          picture: 'https://picsum.photos/200/300',
          expiredAt: null,
          isActive: true,
          createdAt: new Date(),
          updatedAt: new Date()
      },
      {
          id: 2,
          name: 'Product 2',
          qty: 20,
          picture: 'https://picsum.photos/200/300',
          expiredAt: null,
          isActive: false,
          createdAt: new Date(),
          updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

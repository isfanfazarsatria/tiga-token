const db = require('../../models');

const Products = db.product;


//add product
const addProduct = async (req, res) => {
    try {
        const product = await Products.create(req.body);
        res.status(201).send(product);
    }
    catch (err) {
        res.status(400).send(err);
    }
}

//get all product
const getAllProduct = async (req, res) => {
    const product = await Products.findAll();
    res.status(200).send(product);
}

//get product by id
const getProductById = async (req, res) => {
    let id = req.params.id;
    const product = await Products.findOne({
        where: {
            id: id
        }
    });
    res.status(200).send('show product by id',product);
}

//update product
const updateProduct = async (req, res) => {
    let id = req.params.id;
    let info = {
        name: req.body.name,
        qty: req.body.qty,
        picture: req.body.picture,
        expiredAt: req.body.expiredAt,
        // isActive: req.body.isActive ? req.body.isActive : false
    }
    const product = await Products.update(info, {
        where: {
            id: id
        }
    });
    res.status(200).send('success to update your ptoduct',product);
}

//delete product by id
const deleteProduct = async (req, res) => {
    let id = req.params.id;
    const product = await Products.destroy({
        where: {
            id: id
        }
    });
    res.status(200).send('success to delete yor product',product);
}

//get isActive product
const getIsActiveProduct = async (req, res) => {
    const product = await Products.findAll({
        where: {
            isActive: true
        }
    });
    res.status(200).send('show me isActive product',product);
}

module.exports = {
    addProduct,
    getAllProduct,
    getProductById,
    updateProduct,
    deleteProduct,
    getIsActiveProduct
}
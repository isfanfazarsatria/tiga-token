const productController = require('../../src/controllers/productController');
const router = require('express').Router();

router.post('/add', productController.addProduct);
router.get('/', productController.getAllProduct);
router.get('/isactive', productController.getIsActiveProduct);
router.get('/:id', productController.getProductById);
router.put('/:id', productController.updateProduct);
router.delete('/:id', productController.deleteProduct);

module.exports = router;

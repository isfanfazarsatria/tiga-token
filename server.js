const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

var corsOptions = {
    origin: 'http://localhost:4200'
};

//PORT
const PORT = process.env.PORT || 4000;

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

//test api
app.get('/', (req, res) => {
    res.send('Tiga Token API');
});

//router
const productRoute = require('./src/routes/productRoute');
app.use('/api/product', productRoute);

//server listen
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});